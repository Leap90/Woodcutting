package com.leap;

import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.*;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.runetek.event.listeners.HitsplatListener;
import org.rspeer.runetek.event.listeners.NpcSpawnListener;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.HitsplatEvent;
import org.rspeer.runetek.event.types.NpcSpawnEvent;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;

import java.awt.*;

@ScriptMeta(developer = "Leap", name = "Woodcutter", desc = "Will a tree of your choice near a bank")
public class Main extends Script implements HitsplatListener, RenderListener, NpcSpawnListener {

    ExpTracker expTracker = new ExpTracker();

    public static Trees trees;
    private String[] Axes = {"Bronze axe", "Iron axe", "Steel axe", "Black axe", "Mithril axe", "Adamant axe", "Rune axe", "Dragon axe", "Infernal Axe"};
    private Player local = Players.getLocal();
    private boolean bAttacked = false;
    private boolean npcRandom = false;
    private long startTime = System.currentTimeMillis();
    private int startExp = Skills.getExperience(Skill.WOODCUTTING);
    private int startLvl = Skills.getLevel(Skill.WOODCUTTING);


    @Override
    public void onStart(){
        new WoodGUI().setVisible(true);
    }

    /*Main Script Loop*/
    public int loop() {
        if(trees != null) {
            Bank();
            Attack();
            HandleRandoms();
            ChopTree();
            SpecialAttack();
        }
        return Random.nextInt(400, 600);
    }

    /*Check losing health event (self)*/
    @Override
    public void notify(HitsplatEvent e){
        if(e.getSource() == local)
            bAttacked = true;
    }

    /*Check for random spawn*/
    @Override
    public void notify(NpcSpawnEvent e){
        if(e.getSource().getName().equals("Genie") && e.getSource().getTargetIndex() == local.getIndex()) {
            Log.info("Found Genie");
            npcRandom = true;
        }else if(e.getSource().getName().equals("Rick Turpentine") && e.getSource().getTargetIndex() == local.getIndex()){
            Log.info("Found Rick");
            npcRandom = true;
        }
    }

    /*Handle random npc events*/
    private void HandleRandoms(){
        if (npcRandom) {
            Npc eventNpc = Npcs.getNearest(npc -> npc.isPositionInteractable() && npc.isPositionWalkable());
                npcRandom = false;
                if (!Dialog.isOpen()) {
                    eventNpc.interact(x -> true);
                    Time.sleepUntil(Dialog::isOpen, 1000, 10000);
                }
                if (Dialog.canContinue()) {
                    Dialog.processContinue();
                    Time.sleep(500, 2000);
                }
            if(Inventory.contains("Lamp")){
                if(!Interfaces.isOpen(134)) {
                    Inventory.getFirst("Lamp").interact("Rub");
                    Time.sleepUntil(() -> Interfaces.isOpen(134), 500, 10000);
                }
            }
        }
    }

    /*Bank Inventory*/
    private int Bank(){
        if (Inventory.isFull()) {
            if (!Bank.isOpen()) {
                BankLocation bank = BankLocation.getNearest();
                if (bank == null) {
                    Log.info("Cant find bank");
                    return -1;
                } else {
                    Log.info("Open Bank");
                    Bank.open();
                    Time.sleepUntil(Bank::isOpen, Random.nextInt(5000, 40000));
                    Time.sleep(1000, 4000);
                    Bank.depositAllExcept(Axes);
                    Time.sleep(1000, 4000);
                    Time.sleepUntil(Inventory::isEmpty, Random.nextInt(1000, 40000));
                }
            }
            if (Bank.isOpen() && !Inventory.isFull()) {
                Log.info("Close Bank");
                Bank.close();
                Time.sleepUntil(Bank::isClosed, Random.nextInt(1000, 5000));
            }
        }
        return Random.nextInt(400, 600);
    }

    /*Attack the npc attacking you*/
    private void Attack() {
        if (bAttacked) {
            Npc npc = Npcs.getNearest(x -> x.isAnimating() && x.isPositionInteractable() && x.getTarget().equals(local));
            if (npc != null) {
                npc.interact("Attack");
                Log.info("Attacking");
            }
            Time.sleepUntil(local::isAnimating, Random.nextInt(4000, 5000));
        }
    }

    /*Chop tree action*/
    private void ChopTree(){
        SceneObject tree = SceneObjects.getNearest(Main.trees.getName());
        if (tree != null && !local.isAnimating() && !local.isMoving() && !Inventory.isFull()) {
            checkRunToggle();
            Log.info("Chop Tree");
            tree.interact("Chop down");
            Time.sleep(500, 5000);
        } else if(tree == null) {
            Log.info("Trees not found, pausing client, please relocate!");
            onPause();
        }
    }

    /*Activate run*/
    private void checkRunToggle() {
        if (!Movement.isRunEnabled() && Movement.getRunEnergy() > Random.nextInt(20, 35)) {
            Movement.toggleRun(true);
            Log.info("Toggled running on");
            Time.sleep(350, 1200);
        }
    }

    private void SpecialAttack(){
        if(EquipmentSlot.MAINHAND.getItem().getName().equals(Axes[8]) || EquipmentSlot.OFFHAND.getItem().getName().equals(Axes[8]) && Combat.getSpecialEnergy() == 100 && SceneObjects.getNearest(trees.getName()).isPositionInteractable())
            Log.info("Using special Attack");
        Combat.toggleSpecial(true);
    }

    /*Render exp counter*/
    @Override
    public void notify(RenderEvent renderEvent) {
        final long upTime = System.currentTimeMillis() - startTime;
        Skill woodSkill = Skill.WOODCUTTING;

        Graphics g = renderEvent.getSource();
        Graphics2D g2 = (Graphics2D)g;

        g2.setColor(new Color(0, 0, 0, 0.40f));
        g2.setColor(Color.GREEN);
        g2.drawString("Current Level: " + Skills.getLevel(woodSkill) + ("(" + expTracker.GainedLvl(woodSkill, startLvl) + ")"), 5, 290);
        g2.drawString("Exp Gained: " + expTracker.GainedExp(woodSkill, startExp), 5, 305);
        g2.drawString("Exp To Level: " + expTracker.ExpToLvl(woodSkill), 5, 320);
        g2.drawString("Exp/Hour: " + expTracker.ExpHour(woodSkill, startExp, upTime), 5, 335);
        g2.drawString("To Level: " + expTracker.PercentToLvl(woodSkill), 5, 350);
    }

}