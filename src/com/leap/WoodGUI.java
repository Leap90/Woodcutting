package com.leap;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WoodGUI extends JFrame {

private JComboBox treeComboBox;
private JButton Start;


    public WoodGUI(){
        super("Config");

        setLayout(new FlowLayout());

        treeComboBox = new JComboBox(Trees.values());
        Start = new JButton("Start");

        add(treeComboBox);
        add(Start);

        Start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.trees = (Trees) treeComboBox.getSelectedItem();
                setVisible(false);
            }
        });

        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setLocationRelativeTo(null);
        pack();
    }
}
