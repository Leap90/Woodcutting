package com.leap;

public enum Trees {
    TREE("Tree", 1),
    OAK("Oak", 15),
    WILLOW("Willow", 30),
    MAPLE("Maple", 45),
    YEW("Yew", 60),
    MAGIC("Magic", 75);

    private final String name;
    private final int levelReq;

    Trees(final String name, final int levelReq){
        this.name = name;
        this.levelReq = levelReq;
    }

    public String getName(){ return name;}

    public int levelReq(){ return levelReq;}
}
