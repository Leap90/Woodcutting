package com.leap;


import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;

public class ExpTracker {

    public String ExpHour(Skill skill, double startExp, long upTime){
        double gainedExp = Skills.getExperience(skill) - startExp;
        return String.format("%.1f", (gainedExp * 3600000 / upTime) / 1000) + "k";
    }

    public String ExpToLvl(Skill skill){
        return String.format("%.1f", (double)Skills.getExperienceToNextLevel(skill) / 1000) + "k";
    }

    public String GainedExp(Skill skill, int startExp){
        return String.format("%.1f", ((double) Skills.getExperience(skill) - startExp) / 1000) + "k";
    }

    public  int GainedLvl(Skill skill, int startLvl){
        int currentLvl = Skills.getLevel(skill);
        return currentLvl - startLvl;
    }

    public String PercentToLvl(Skill skill){
        long currentExp = Skills.getExperience(skill);
        int currentLvl = Skills.getLevel(skill);
        long expNextLvl = Skills.getExperienceAt(currentLvl);
        long expAtLvl = Skills.getExperienceAt(currentLvl + 1);
        return (((expNextLvl - currentExp) * 100) / (expNextLvl - expAtLvl)) + "%";
    }

}
